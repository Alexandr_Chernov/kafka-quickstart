from kafka import KafkaConsumer
import time


consumer = KafkaConsumer('quickstart', bootstrap_servers='localhost:57186', api_version=(0, 11, 5))

message_count = 0
for _ in consumer:
    message_count += 1
    print(f"Сообщение №: {message_count}")
    if message_count >= 1000:
        break
    time.sleep(0.1)

consumer.close()
